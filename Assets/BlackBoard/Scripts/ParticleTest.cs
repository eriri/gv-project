﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Profiling;
using System;
using System.Runtime.InteropServices;


public class ParticleTest : MonoBehaviour {

    #region Data Structure
    struct EmitParticleInfo
    {
        public uint emitCount;
        public float lifespan;
        public float _dt;
        public Vector3 scale;
        public float initVelocity;
        public Vector3 originalPos;
        public float radius;
        public Vector3 acceleration;
        public Vector3 prevPosition;
        public Vector3 angularSpeed;

    }

    struct ParticleCounter
    {
        public uint alivelistCount;
        public uint deadlistCount;
        public uint updateParticleCount;
    }


    struct Particle
    {
        public float lifespan;
        public Vector3 position;
        public Vector3 velocity;
        public Matrix4x4 model;
        public Vector3 scale;
        public Vector4 quaternion;
    }
    #endregion

    #region Public Variable
    public float emitRate = 0;

    [SerializeField]
    int _maxParticle;

    public int maxParticle
    {
        get { return _maxParticle; }
        set { _maxParticle = value; _reset = true; }
    }

    [SerializeField]
    public Mesh _mesh;
    public Material _material;

    public ComputeShader computeShader;

    public Boolean click = true;
    public Boolean freeze = false;

    public Transform sphere;

    public Slider accY;
    public Slider accX;
    public Slider turb;
    #endregion

    #region Private Variable
    int initBufferCSID;
    int emitCountCSID;
    int emitParticleCSID;
    int updateParticleCSID;
    int setDrawBufferArgCSID;

    float emitCount;

    private float lifespan = 2;

    private float startVelocity = 4;
    private Vector3 acceleration;

    private float radius = 0.3f;
    private Vector3 rotation = new Vector3(200, 200, 200);

    private float pos1 = 33.32f;
    private float pos2 = 30.32f;
    private float speed = 2f;

    private bool receiveShadow = true;
    private bool castShadow = true;

    MaterialPropertyBlock mpb;

    ComputeBuffer particlesCB;
    ComputeBuffer emitParticleInfoCB;

    ComputeBuffer particlePoolCB;

    ComputeBuffer aliveListCB;
    ComputeBuffer aliveListSwapCB;

    ComputeBuffer particleCounterCB;

    ComputeBuffer instancingArgCB;
    ComputeBuffer updateIndirectCB;

    int particlesId = Shader.PropertyToID("_particles");
    int emitParticleInfoId = Shader.PropertyToID("_emitParticleInfo");

    int deadListId = Shader.PropertyToID("_deadList");
    int particlePoolId = Shader.PropertyToID("_particlePool");

    int aliveListId = Shader.PropertyToID("_aliveList");
    int aliveListReadId = Shader.PropertyToID("_aliveListRead");

    int aliveListSwapId = Shader.PropertyToID("_aliveListSwap");

    int particleCounterId = Shader.PropertyToID("_particleCounter");

    int updateIndirectBufferId = Shader.PropertyToID("_updateIndirectBuffer");

    int instancingArgId = Shader.PropertyToID("_instancingArg");

    int timeId = Shader.PropertyToID("_time");

    int sphereId = Shader.PropertyToID("_sphere");

    int turbId = Shader.PropertyToID("_turb");

    uint[] args = new uint[5] { 0, 0, 0, 0, 0 };
    bool _reset = false;

    int deadListCount;
    int aliveListCount;

    Vector3 prevPosition;
    EmitParticleInfo[] emitInfoParam;
    #endregion

    #region MonoBehaviour
    // Use this for initialization
    void Start () {
        InitBuffer();
    }

    void OnEnable()
    {
        prevPosition = transform.position;

    }

    void OnDestroy()
    {
        DisposeBuffer();   
    }
    #endregion
    
    // Initialize compute buffers
    void InitBuffer()
    {
        // Size need to be that of struct
        particlesCB = new ComputeBuffer(_maxParticle, System.Runtime.InteropServices.Marshal.SizeOf(typeof(Particle)));
        emitParticleInfoCB = new ComputeBuffer(1, System.Runtime.InteropServices.Marshal.SizeOf(typeof(EmitParticleInfo)));

        particlePoolCB = new ComputeBuffer(_maxParticle, sizeof(uint), ComputeBufferType.Append);

        aliveListCB = new ComputeBuffer(_maxParticle, sizeof(uint), ComputeBufferType.Append);
        aliveListSwapCB = new ComputeBuffer(_maxParticle, sizeof(uint), ComputeBufferType.Append);

        particleCounterCB = new ComputeBuffer(1, System.Runtime.InteropServices.Marshal.SizeOf(typeof(ParticleCounter)));

        instancingArgCB = new ComputeBuffer(1, args.Length * sizeof(uint), ComputeBufferType.IndirectArguments);
        updateIndirectCB = new ComputeBuffer(1, 3 * sizeof(uint), ComputeBufferType.IndirectArguments);

        aliveListCB.SetCounterValue(0);

        particlePoolCB.SetCounterValue(0);

        particleCounterCB.SetData(new ParticleCounter[] { new ParticleCounter() });

        updateIndirectCB.SetData(new uint[3] { 1, 1, 1 });
        
        initBufferCSID = computeShader.FindKernel("Init");
        emitParticleCSID = computeShader.FindKernel("Emit");
        emitCountCSID = computeShader.FindKernel("EmitCount");
        updateParticleCSID = computeShader.FindKernel("Update");
        setDrawBufferArgCSID = computeShader.FindKernel("SetDrawBufferArg");

        DispatchInit();
        _reset = false;
    }

    void ResetBuffer()
    {
        DisposeBuffer();
        InitBuffer();
    }

    void DisposeBuffer()
    {
        particlesCB.Dispose();
        aliveListCB.Dispose();
        emitParticleInfoCB.Dispose();
        particlePoolCB.Dispose();
        particleCounterCB.Dispose();
        aliveListSwapCB.Dispose();
        instancingArgCB.Dispose();
        updateIndirectCB.Dispose();
    }

    void SetEmitInfoBuffer()
    {
        if (emitInfoParam == null)
        {
            emitInfoParam = new EmitParticleInfo[1];
        }

        // Set emitter Data
        emitInfoParam[0].emitCount = (uint)emitCount;
        emitInfoParam[0].lifespan = lifespan;
        emitInfoParam[0]._dt = Time.deltaTime *(float) 0.2;
        emitInfoParam[0].originalPos = transform.position;
        emitInfoParam[0].initVelocity = startVelocity;
        emitInfoParam[0].acceleration = acceleration;
        emitInfoParam[0].scale = transform.localScale;
        emitInfoParam[0].prevPosition = prevPosition;
        emitInfoParam[0].radius = radius;
        emitInfoParam[0].angularSpeed.Set(rotation.x * Mathf.Deg2Rad, rotation.y * Mathf.Deg2Rad, rotation.z * Mathf.Deg2Rad);
        emitParticleInfoCB.SetData(emitInfoParam);
    }

    #region Compute Shader Dispatch
    void DispatchInit()
    {
        int kernelId = initBufferCSID;

        particlePoolCB.SetCounterValue(0);

        // Assigned to dead list & populate particlePoolCB
        computeShader.SetBuffer(kernelId, deadListId, particlePoolCB);
        computeShader.SetBuffer(kernelId, particleCounterId, particleCounterCB);

        computeShader.Dispatch(kernelId, _maxParticle, 1, 1);
    }

    // Update Stage 1
    // Calculate how many particle can be emitted
    void DispatchEmitCount()
    {
        ComputeShader cs;
        int kernelId;
        cs = computeShader;
        kernelId = emitCountCSID;
        if (cs == null) return;

        cs.SetBuffer(kernelId, particleCounterId, particleCounterCB);
        cs.SetBuffer(kernelId, emitParticleInfoId, emitParticleInfoCB);
        cs.SetBuffer(kernelId, updateIndirectBufferId, updateIndirectCB);
        cs.Dispatch(kernelId, 1, 1, 1);
    }

    // Update Stage 2
    // Emit particle
    void DispatchEmit()
    {
        ComputeShader cs;
        int kernelId;
        cs = computeShader;
        kernelId = emitParticleCSID;
        if (cs == null) return;

        cs.SetBuffer(kernelId, particlesId, particlesCB);
        cs.SetBuffer(kernelId, emitParticleInfoId, emitParticleInfoCB);

        // Assigned to particle pool & remove from particlePoolCB
        cs.SetBuffer(kernelId, particlePoolId, particlePoolCB);
        cs.SetBuffer(kernelId, aliveListId, aliveListCB);
 
        cs.SetBuffer(kernelId, particleCounterId, particleCounterCB);
        cs.SetFloat(timeId, Time.time);
        cs.Dispatch(kernelId, (int)Mathf.Ceil(emitCount / 1024.0f), 1, 1);
    }

    // Update Stage 3
    // Update particle
    void DispatchUpdate()
    {
        ComputeShader cs;
        int kernelId;
        uint numIndices = (_mesh != null) ? (uint)_mesh.GetIndexCount(0) : 0;
        args[0] = numIndices;
        args[1] = 0;
        instancingArgCB.SetData(args);

        cs = computeShader;
        aliveListSwapCB.SetCounterValue(0);
        kernelId = updateParticleCSID;
        if (cs == null) return;


        cs.SetVector(sphereId, new Vector4(sphere.position.x, sphere.position.y, sphere.position.z, sphere.localScale.x * 0.5f));
        cs.SetFloat(turbId, turb.value);

        cs.SetBuffer(kernelId, particlesId, particlesCB);
        cs.SetBuffer(kernelId, emitParticleInfoId, emitParticleInfoCB);

        cs.SetBuffer(kernelId, particleCounterId, particleCounterCB);

        // Assigned to dead list & populate particlePoolCB again particle dies
        cs.SetBuffer(kernelId, deadListId, particlePoolCB);

        cs.SetBuffer(kernelId, aliveListReadId, aliveListCB);
        cs.SetBuffer(kernelId, aliveListSwapId, aliveListSwapCB);

        cs.SetBuffer(kernelId, instancingArgId, instancingArgCB);
        cs.DispatchIndirect(kernelId, updateIndirectCB, 0);
    }

    // Update Stage 4
    // Update instancing indirect buffer
    void DispatchDrawArg()
    {
        ComputeShader cs;
        int kernelId;
        cs = computeShader;
        kernelId = setDrawBufferArgCSID;
        if (cs == null) return;

        cs.SetBuffer(kernelId, particleCounterId, particleCounterCB);
        cs.SetBuffer(kernelId, instancingArgId, instancingArgCB);
        cs.Dispatch(kernelId, 1, 1, 1);
    }
    #endregion

    #region Public Method
    public void WantReset()
    {
        _reset = true;
    }

    public void WantEmit(int num)
    {
        if (num > _maxParticle) num = _maxParticle;
        emitCount += num;
    }

    public void switchClick()
    {
        click = !click;
    }

    public void switchFreeze()
    {
        freeze = !freeze;
    }
    #endregion

    #region MonoBehaviour Update
    void FixedUpdate()
    {

        acceleration.x = accX.value;
        acceleration.y = accY.value;

        emitCount += emitRate * Time.deltaTime;

        if (_reset)
        {
            ResetBuffer();
            return;
        }

        // Set emitter Data
        if (emitInfoParam == null)
        {
            emitInfoParam = new EmitParticleInfo[1];
        }

        SetEmitInfoBuffer();

        emitParticleInfoCB.SetData(emitInfoParam);

        if (Input.GetMouseButton(0) && click)
        {
            RaycastHit hit;
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit))
            {
                //emitCount += emitRate * Time.deltaTime;
                emitInfoParam[0].originalPos = hit.point;
                emitParticleInfoCB.SetData(emitInfoParam);

                
                DispatchEmitCount();
                if (emitCount > 0) DispatchEmit();

                prevPosition = hit.point; 
                emitCount -= (uint)emitCount;

            }
        }
        else if (!click)
        {
            SetEmitInfoBuffer();
            DispatchEmitCount();
            if (emitCount > 0) DispatchEmit();
        }

        if (!freeze)
        {
            DispatchUpdate();

            // Update alive list
            Swap<ComputeBuffer>(ref aliveListCB, ref aliveListSwapCB);
            DispatchDrawArg();
        }

        if (!click)
        {
            prevPosition = transform.position;
            emitCount -= (uint)emitCount;
        }

    }

    void Update()
    {

        //Translate emitter
        float z = Mathf.Lerp(pos1, pos2, (Mathf.Sin(speed * Time.time) + 1.0f) / 2.0f);
        transform.position = new Vector3(transform.position.x, transform.position.y, z);

        if (_mesh == null || _material == null) return;

        // Pass data to shader
        if (mpb == null) { mpb = new MaterialPropertyBlock(); }
        mpb.SetBuffer(aliveListId, aliveListCB);
        mpb.SetBuffer(particlesId, particlesCB);
        mpb.SetBuffer(emitParticleInfoId, emitParticleInfoCB);

        Graphics.DrawMeshInstancedIndirect(_mesh, 0, _material, new Bounds(Vector3.zero, new Vector3(10000.0f, 10000.0f, 10000.0f)), instancingArgCB, 0, mpb, castShadow ? UnityEngine.Rendering.ShadowCastingMode.On : UnityEngine.Rendering.ShadowCastingMode.Off, receiveShadow);
    }
    #endregion

    #region Utility
    static void Swap<T>(ref T lhs, ref T rhs)
    {
        T temp;
        temp = lhs;
        lhs = rhs;
        rhs = temp;
    }
    #endregion
}
