
struct EmitParticleInfo
{
	uint emitCount;
	float lifespan;
	float _dt;
	float3 scale;
	float initVelocity;
	float3 originalPos;
	float radius;
	float3 acceleration;
	float3 prevPosition;
	float3 angularSpeed;
};

struct ParticleCounter{
  uint alivelistCount;
  uint deadlistCount;
  uint updateParticleCount;
};

struct Particle {
	float lifespan;
	float3 position;
	float3 velocity;
	float4x4 model;
	float3 scale;
	float4 quaternion;
};

struct IndirectArgumentBuffer
{
	uint arg[5];
};

struct IndirectDispatchBuffer
{
	int arg[3];
};
