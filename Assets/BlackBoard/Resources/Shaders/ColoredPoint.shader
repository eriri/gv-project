﻿Shader "Custom/ColoredPoint" {
	Properties{
		//_Color ("Color", Color) = (1,1,1,1)
		//_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Glossiness("Smoothness", Range(0,1)) = 0.98
		_Metallic("Metallic", Range(0,1)) = 0.5
		_EmissionLM("Emission (Lightmapper)", Float) = 0
		[Toggle] _DynamicEmissionLM("Dynamic Emission (Lightmapper)", Int) = 0

	}
		SubShader{
			Tags { "RenderType" = "Opaque" }
			LOD 200
			Blend SrcAlpha one

			CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard addshadow fullforwardshadows
		#pragma multi_compile_instancing
		#pragma instancing_options procedural:setup

		#include "UnityInstancing.cginc"
		#include "Particle.cginc"
		#include "UnityCG.cginc"

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		//sampler2D _MainTex;

		struct Input {
		//float2 uv_MainTex;
	  float3 worldPos;
	};

	half _Glossiness;
	half _Metallic;
	fixed4 _Color;

#ifdef UNITY_PROCEDURAL_INSTANCING_ENABLED
			StructuredBuffer<Particle> _particles;
			StructuredBuffer<uint> _aliveList;
			StructuredBuffer<EmitParticleInfo> _emitParticleInfo;
	#endif

			void setup()
			{
	#ifdef UNITY_PROCEDURAL_INSTANCING_ENABLED
				uint particleId = _aliveList[unity_InstanceID];
				Particle data = _particles[particleId];

				unity_ObjectToWorld = data.model;
				unity_WorldToObject = unity_ObjectToWorld;
				unity_WorldToObject._14_24_34 *= -1;
				unity_WorldToObject._11_22_33 = 1.0f / unity_WorldToObject._11_22_33;
	#endif
			}

			UNITY_INSTANCING_BUFFER_START(Props)

			UNITY_INSTANCING_BUFFER_END(Props)

			void surf(Input IN, inout SurfaceOutputStandard o) {

	#ifdef UNITY_PROCEDURAL_INSTANCING_ENABLED
				uint particleId = _aliveList[unity_InstanceID];
				Particle data = _particles[particleId];

				//o.Albedo.r = 1.0f - lerpVal + 0.1;// 1;//+ 0.2 * data.lifespan / _emitParticleInfo[0].lifespan;
				//o.Albedo.g = lerpVal + 0.1; //data.lifespan / _emitParticleInfo[0].lifespan;
				//o.Albedo.b = lerpVal + 0.1; //+0.2 * data.lifespan / _emitParticleInfo[0].lifespan;

				float lerp = data.lifespan * 0.4f;
				_Color = fixed4(1.0f - lerp + 0.1, lerp + 0.1, 1.0f, lerp);
	#endif

				/*o.Albedo.r = IN.worldPos.x / 2;
				o.Albedo.g = IN.worldPos.y / 2;
				o.Albedo.b = IN.worldPos.z / 2;*/

				//fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
				o.Alpha = 1;
				o.Albedo = _Color.rgb;
				//o.Albedo.rgb = IN.worldPos.xyz * 0.5 + 0.5;

				o.Metallic = _Metallic;
				o.Smoothness = _Glossiness;
				o.Emission = o.Albedo * 0.1;

			}
			ENDCG
	}
		FallBack "Diffuse"
}
